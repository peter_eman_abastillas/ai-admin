export default {
  MAX_ATTACHMENT_SIZE: 5000000,
  s3: {
    BUCKET: 'notes-app-uploads'
  },
  apiGateway: {
    URL: 'https://xuutzb3el1.execute-api.us-east-2.amazonaws.com/prod',
    REGION: 'us-east-2',
    SERVICE: {
      NOTES: "/test-rest"
    }
  },
  cognito: {
    REGION: 'us-east-2',
    IDENTITY_POOL_ID: 'us-east-2:6e7b8972-2ed7-46b4-a740-16132504eacc',
    USER_POOL_ID : 'us-east-2_g5KQWNye0',
    APP_CLIENT_ID : '6t7qprritit3gri6jmr6to24m4',
  }
};
