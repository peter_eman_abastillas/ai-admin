import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  FormGroup,
  FormControl,
  ControlLabel,
} from 'react-bootstrap';
import { invokeApig, s3Upload } from '../libs/awsLib';
import LoaderButton from '../components/LoaderButton';
import config from '../config.js';
import './Notes.css';

class Notes extends Component {
  constructor(props) {
    super(props);

    this.file = null;

    this.state = {
      isLoading: null,
      isDeleting: null,
      note: null,
      text: '',
    };
  }

  async componentDidMount() {
    try {
      const results = await this.getNote();
      this.setState({
        note: results,
        text: results.text,
      });
    }
    catch(e) {
      alert(e);
    }
  }

  getNote() {
    return invokeApig({ uriParams: this.props.match.params.id }, this.props.userToken);
  }

  validateForm() {
    return this.state.text.length > 0;
  }


  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleFileChange = (event) => {
    this.file = event.target.files[0];
  }

  saveNote(note) {
    return invokeApig({
      uriParams: this.props.match.params.id,
      method: 'POST',
      body: note,
    }, this.props.userToken);
  }

  handleSubmit = async (event) => {
    event.preventDefault();


    this.setState({ isLoading: true });

    try {

      await this.saveNote({
        ...this.state.note,
        text: this.state.text,
      });
      this.props.history.push('/');
    }
    catch(e) {
      alert(e);
      this.setState({ isLoading: false });
    }
  }

  deleteNote() {
    return invokeApig({
      uriParams: this.props.match.params.id,
      method: 'DELETE',
    }, this.props.userToken);
  }

  handleDelete = async (event) => {
    event.preventDefault();

    const confirmed = window.confirm('Are you sure you want to delete this note?');

    if ( ! confirmed) {
      return;
    }

    this.setState({ isDeleting: true });

    try {
      await this.deleteNote();
      this.props.history.push('/');
    }
    catch(e) {
      alert(e);
      this.setState({ isDeleting: false });
    }
  }

  render() {
    return (
      <div className="Notes">
        { this.state.note &&
          ( <form onSubmit={this.handleSubmit}>
              <FormGroup controlId="text">
                <FormControl
                  onChange={this.handleChange}
                  value={this.state.text}
                  componentClass="textarea" />
              </FormGroup>


              <LoaderButton
                block
                bsStyle="primary"
                bsSize="large"
                disabled={ ! this.validateForm() }
                type="submit"
                isLoading={this.state.isLoading}
                text="Save"
                loadingText="Saving…" />
              <LoaderButton
                block
                bsStyle="danger"
                bsSize="large"
                isLoading={this.state.isDeleting}
                onClick={this.handleDelete}
                  text="Delete"
                  loadingText="Deleting…" />
            </form> )}
        </div>
      );
  }
}

export default withRouter(Notes);
